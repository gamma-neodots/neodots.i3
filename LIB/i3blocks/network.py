#!/usr/bin/env python3
"""A NetworkManager i3blocks blocklet"""
import os
import subprocess
import ipaddress

COLORKEY = {
    'wifi': {
        'disconnected': '#dc322f',
        'connected': '#85c000',
        'connecting': '#cccc00',
    },
    'ethernet': {
        'disconnected': '#dc322f',
        'connected': '#859900',
        'connecting': '#b58900',
    },
    'bt': {
        'disconnected': '#dc322f',
        'connected': '#85c000',
        'connecting': '#cccc00',
    },
    'tun': {
        'connected': '#30b8b0',
        'connecting': '#268bd2',
        'disconnected': '#dc322f',
    },
    'loopback': {
        'unmanaged': '#93a1a1'
    }
}

class NMDev:
    """encapsulate NetworkManager devices"""
    name = ''
    type_ = None
    connection = None
    address = None
    state = None
    color = None
    def __init__(self):
        pass
    def __str__(self):
        return ''.join([
            '[not connected] ' if not self.state else '',
            f'{self.type_}|' if self.type_ is not None else '',
            f'{self.connection} ' if self.connection != "--" else '',
            f'{self.name} ' if self.name is not None else '',
            f'{self.address.ip}' if self.address is not None else '',
        ])

    def icon(self, small=False):
        """get NerdFont icon, given device type"""
        if self.type_ == 'wifi':
            icon = ''
        elif self.type_ == 'ethernet':
            icon = ''
        elif self.type_ == 'bt':
            icon = ''
        elif self.type_ == 'tun':
            icon = '嬨'
        return icon if small else '<span size="large">' + icon + '</span>'

    def block_text(self):
        """build full block text for this device"""
        parts = []
        ends = []
        if self.color is not None:
            parts.append('<span foreground="{}">'.format(self.color))
            ends.insert(0, '</span>')
        parts.append(self.icon())
        if self.connection != '--' and self.type_ != 'ethernet':
            parts.append(f' {self.connection}')
        parts.append('<small> ')
        ends.insert(0, '</small>')
        if self.address is not None:
            parts.append(str(self.address.ip))
        parts.extend(ends)
        return ''.join(parts)

    def short_text(self):
        """build short block text for this device"""
        parts = []
        ends = []
        if self.color is not None:
            parts.append('<span foreground="{}">'.format(self.color))
            ends.insert(0, '</span>')
        parts.append(self.icon(small=True))
        parts.append('<small> ')
        ends.insert(0, '</small>')
        if self.connection != '--':
            parts.append(f'{self.connection} ')
        parts.extend(ends)
        return ''.join(parts)

BUTTON = os.environ.get('BLOCK_BUTTON')
if BUTTON == '1':
    subprocess.Popen(['nohup', 'nm-connection-editor'],
                     stdout=open('/dev/null', 'w'),
                     stderr=open('/dev/null', 'w'),
                     preexec_fn=os.setpgrp
                    )
elif BUTTON == '3':
    subprocess.Popen(['nohup', 'sys-notif', 'ip'],
                     stdout=open('/dev/null', 'w'),
                     stderr=open('/dev/null', 'w'),
                     preexec_fn=os.setpgrp
                    )

devs = []
with subprocess.Popen(['nmcli', 'device', 'show'], stdout=subprocess.PIPE) as p:
    device = NMDev()
    for line in iter(p.stdout.readline, ''):
        # end of input
        if line == b'':
            break
        # end of current device
        if line == b'\n':
            if device is not None:
                devs.append(device)
            device = NMDev()
            color = None
        line = line.decode()
        if 'DEVICE' in line:
            device.name = line.split()[1]
        elif 'TYPE' in line:
            device.type_ = line.split()[1]
            for key, value in COLORKEY.items():
                if key in line:
                    color = value
                    break
        elif '4.ADDRESS' in line:
            device.address = ipaddress.ip_interface(line.split()[1])
        elif 'CONNECTION' in line:
            device.connection = ' '.join(line.split()[1:])
        elif 'STATE' in line:
            device.state = line.split()[2]
            if color is not None:
                for key, value in color.items():
                    if key in line:
                        device.color = value
                        break

    devs.append(device)

    print(' '.join(d.block_text() for d in devs if d.type_ != 'loopback'))
    print(''.join(d.short_text() for d in devs if d.type_ != 'loopback'))
