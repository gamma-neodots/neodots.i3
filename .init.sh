#!/usr/bin/env bash
ldir "CONFIG" "${XDG_CONFIG_HOME:-"$HOME/.config"}"
ldir "LIB" "$HOME/.local/lib"
ldir_bin "BIN" "$HOME/.local/bin"
